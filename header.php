<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">
    <?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>
<?php wp_body_open(); ?>


<header>
    <nav>
        <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri();?>/medias/logos/logo.png" alt="logo de Spotify"></a>
        <a href="<?php echo get_category_link(2); ?>"><?php  echo get_cat_name(2); ?></a>
        <a href="<?php echo get_category_link(3); ?>"><?php  echo get_cat_name(3); ?></a>
        <a href="<?php echo get_category_link(4); ?>"><?php  echo get_cat_name(4); ?></a>
        <a href="<?php echo get_category_link(5); ?>"><?php  echo get_cat_name(5); ?></a>
        <a href="<?php echo get_category_link(6); ?>"><?php  echo get_cat_name(6); ?></a>
        <a href="<?php echo get_category_link(7); ?>"><?php  echo get_cat_name(7); ?></a>
        <a href="<?php echo get_category_link(8); ?>"><?php  echo get_cat_name(8); ?></a>
        <a href="<?php echo get_category_link(9); ?>"><?php  echo get_cat_name(9); ?></a>
        <a href="<?php echo get_category_link(1); ?>"><?php  echo get_cat_name(1); ?></a>
    </nav>
    <h1><?php echo get_the_title() ?></h1>
</header>

<?php
add_theme_support('title-tag'); // retirer balise title pour que ça fonctionne (ds header.php)

// fonction qui appelle le fichier style.css
wp_enqueue_style('style.css', get_stylesheet_uri()); //permet de récupérer la feuille de style et son chemin
// j'appelle main.js
wp_register_script('main', '/wp-content/themes/sae303/script/main.js'); // dire à wp que main existe
function ajout_script() {
    if(is_single()) {
      wp_enqueue_script('main', get_template_directory_uri() . '/script/main.js');
    }
}
function add_defer_attribute($tag, $handle) {
    if ( 'main.js' !== $handle )
      return $tag;
    return str_replace( 'src', ' defer="defer" src', $tag );
  }
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);
add_action('wp_enqueue_scripts', 'ajout_script');

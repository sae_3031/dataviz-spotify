<?php /* affichage d'un seul post */ ?>

<?php get_header(); ?>

<?php /* si j'ai un post, je l'affiche */ ?>
<?php if( have_posts() ) : the_post(); ?>
    <?php global $post; ?>
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
        <!--lui dire d'aller chercher div avec ce slug, renommer les id avec les slug dans JS-->
        <div id="<?php echo $post->post_name;?>"></div>
    <?php endif; /*j'enlève le while pour n'avoir qu'un seul article sur la page*/ ?>

<?php /* afficher le post suivant et précédent */ ?>
<?php echo "<br>"; ?>
<?php previous_post_link(); ?>
<?php echo "<br>"; ?>
<?php next_post_link(); ?>

<?php get_footer();?>
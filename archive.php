<?php /* informations meta des post */ ?>

<?php get_header(); ?>

<main>
<?php /* http://localhost/wordpress/category/ingredients/ --> voir les articles de cette category w/ auteur et contenu */ ?>
<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>
        <article>
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
            <?php echo "<br>"; ?>
            <a href="<?php the_permalink(); ?>">Lire plus</a> <!--Ajouter le lien vers l'article avec "Lire plus"-->
        </article>
    <?php endwhile; endif; ?>
</main>

<?php get_footer();?>
// Abonnement selon ancienneté

function unpackR(rowsT, key) {
    return (rowsT.map((rowT) => rowT[key] ));
}

 function drawR(divtemps, rowsFree, rowsPremium) {

 const Free = unpackR(rowsFree, 'spotify_usage_period');
 console.log(Free)
 
 const Premium = unpackR(rowsPremium, 'spotify_usage_period');

 let trace1 = {
    x: Free,
    name: 'Free (ad-supported)',
    type: 'histogram' ,
    marker: {
      color: 'hsl(178.47, 75.64%, 30.59%)' ,
      line: {
        color: 'hsl(255, 255%, 255%)'  , 
        width: 1.5,
      },
    },
  };
  
  let trace2 = {
    x: Premium,
    name: 'Premium (paid subscription)',
    type: 'histogram',
    marker: {
      color: 'hsl(169.84, 63.92%, 38.04%)' ,
      line: {
        color: 'hsl(255, 255%, 255%)'  , 
        width: 1.5,
      },
    },
  };

  const layout = {
    width: 800,
    height: 500,
    title: 'Ancienneté sur spotify selon l\'abonnement',
    barmode: 'group',
}
  
  let data = [trace1, trace2];
  
  
  Plotly.newPlot(divtemps, data, layout);
}

async function mainR() {
    const rowsT = await d3.csv("dataset/Spotify_users_data.csv");
    const rowsFree = rowsT.filter((row) => (row.spotify_subscription_plan === 'Free (ad-supported)'));
    const rowsPremium = rowsT.filter((row) => (row.spotify_subscription_plan === 'Premium (paid subscription)'));


    
    
    const Resultat = document.getElementById('temps');
    drawR(Resultat, rowsFree, rowsPremium);
}
mainR();

//Selon le genre


function unpack(rows, key) {
    return (rows.map((row) => row[key] ));
}

function draw(divgenre, rows) {

    const customColors = {
        'Others': 'hsl(200.87, 35.38%, 25.49%)',
        'Female': 'hsl(185.51, 65.33%, 29.41%)',
        'Male': 'hsl(192.68, 48.97%, 28.43%)',

    };

    const Gender = unpack(rows, 'Gender');
    const trace = {
        labels: Gender,
        type: 'pie',
        name: 'Genre',
        marker: {
            colors: Gender.map(value => customColors[value]),
        }
    };

    const layout = {
        width: 800,
        height: 500,
        title: 'Genre',
    }
Plotly.newPlot(divgenre, [trace], layout);
}

async function main() {
    const rows = await d3.csv("dataset/Spotify_users_data.csv");
    console.log(rows);
    const genre = document.getElementById('genre');
    draw(genre, rows);
}

 main();

 //Abonnement 

 function unpackA(rowsA, key) {
    return (rowsA.map((rowA) => rowA[key] ));
}

function drawA(divabonnement, rowsA) {
    const customColors = {
        'Free (ad-supported)': 'hsl(178.47, 75.64%, 30.59%)',
        'Premium (paid subscription)': 'hsl(169.84, 63.92%, 38.04%)',
    };

    const subscription = unpackA(rowsA, 'spotify_subscription_plan');
    const trace = {
        labels: subscription,
        type: 'pie',
        name: 'Abonnement',
        marker: {
            colors: subscription.map((value) => customColors[value]),
        }
    };

    const layout = {
        width: 800,
        height: 500,
        title: 'Abonnement',
    };

Plotly.newPlot(divabonnement, [trace], layout);
}

async function mainA() {
    const rowsA = await d3.csv("dataset/Spotify_users_data.csv");
    console.log(rowsA);
    const abonnement = document.getElementById('abonnement');
    drawA(abonnement, rowsA);
}

 mainA();

 //Abonnement selon le genre 

 function unpackR(rowsR, key) {
    return (rowsR.map((rowR) => rowR[key] ));
}

 function drawR(divR, NombreCategories, ReponsesGenreCalculF, ReponsesGenreCalculP) {

 let trace1 = {
    x: NombreCategories,
    y: Object.values(ReponsesGenreCalculF),
    name: 'Free (ad-supported)',
    type: 'bar',
    marker: {
        color: 'hsl(178.47, 75.64%, 30.59%)' ,
        line: {
          color: 'hsl(255, 255%, 255%)'  , 
          width: 1.5,
        },
      },
  };
  
  let trace2 = {
    x: NombreCategories,
    y: Object.values(ReponsesGenreCalculP),
    name: 'Premium (paid subscription)',
    type: 'bar',
    marker: {
        color: 'hsl(169.84, 63.92%, 38.04%)' ,
        line: {
          color: 'hsl(255, 255%, 255%)'  , 
          width: 1.5,
        },
      },
  };

  const layout = {
    width: 800,
    height: 500,
    title: 'Abonnement selon le genre',
    barmode: 'stack',
}
  
  let data = [trace1, trace2];
  
  
  Plotly.newPlot(divR, data, layout);
}

async function mainR() {
    const rowsR = await d3.csv("dataset/Spotify_users_data.csv");
    const rowsFree = rowsR.filter((row) => (row.spotify_subscription_plan === 'Free (ad-supported)'));
    const rowsPremium = rowsR.filter((row) => (row.spotify_subscription_plan === 'Premium (paid subscription)'));
    
const NombreCategories = Array.from(new Set(unpack(rowsR, 'Gender')));
console.log(NombreCategories);
//Compter le total
const ReponsesGenre = {};
NombreCategories.forEach((genre) => {
    ReponsesGenre[genre] = 0;
    rowsR.forEach((row) => {
if (row.Gender===genre) {
    ReponsesGenre[genre] += 1;
}
    });
});

//Compter le total par type d'abonnement et par genre 

//Abonnement free

const ReponsesGenreFree = {};
NombreCategories.forEach((genre) => {
    ReponsesGenreFree[genre] = 0;
    rowsFree.forEach((row) => {
if (row.Gender===genre) {
    ReponsesGenreFree[genre] += 1;
}
    });
});


//Abonnement Premium

const ReponsesGenrePremium = {};
NombreCategories.forEach((genre) => {
    ReponsesGenrePremium[genre] = 0;
    rowsPremium.forEach((row) => {
if (row.Gender===genre) {
    ReponsesGenrePremium[genre] += 1;
}
    });
});

//Calcul free

const ReponsesGenreCalculF = {};
NombreCategories.forEach((genre) => {
    ReponsesGenreCalculF[genre] = 0;
    rowsFree.forEach((row) => {
if (row.Gender===genre) {
    ReponsesGenreCalculF[genre] = ((ReponsesGenreFree[genre] / ReponsesGenre[genre]) * 100);
}
    });
});

//Calcul Premium

const ReponsesGenreCalculP = {};
NombreCategories.forEach((genre) => {
    ReponsesGenreCalculP[genre] = 0;
    rowsPremium.forEach((row) => {
if (row.Gender===genre) {
    ReponsesGenreCalculP[genre] = ((ReponsesGenrePremium[genre] / ReponsesGenre[genre]) * 100);
}
    });
});
console.log(ReponsesGenreCalculP); 


    const Resultat = document.getElementById('R');
    drawR(Resultat, NombreCategories, ReponsesGenreCalculF, ReponsesGenreCalculP);
}
mainR();

// JS CAMILLE.F

function unpack(rows, key) {
    return (rows.map((row) => row[key]));
  }
  
  function drawListeningContextPourcent(div, pourcentageFree, pourcentagePro, nbUtiContexte) {
    // const xValue = unpack(rows, 'music_lis_frequency');
  
    const trace1 = {
      x: Object.keys(nbUtiContexte),
      y: Object.values(pourcentageFree),
      type: 'bar',
      hovertemplate: '%{y} users Freemium listen %{x} ',
      name: 'Contexte d\'écoute Freemium',
      marker: {
        color: 'rgb(163,165,217)',
        line: {
          color: 'rgb(141,143,188)',
          width: 1.5,
        },
      },
    };
    const trace2 = {
      x: Object.keys(nbUtiContexte),
      y: Object.values(pourcentagePro),
      type: 'bar',
      hovertemplate: '%{y} users Premium listen %{x} ',
      name: 'Contexte d\'écoute Premium',
      marker: {
        color: 'rgb(110,101,166)',
        line: {
          color: 'rgb(98,93,168)',
          width: 1.5,
        },
      },
    };
    const layout = {
      width: 800,
      height: 800,
      title: '<b>Contexte d\'écoute</b> en fonction de <b>l\'abonnement</b>',
      barmode: 'stack',
      bargap: 0.3,
      xaxis: {
        title: '<b>Contexte d\'écoute<b>',
        tickangle: -75,
        automargin: true,
        // type: 'log',
      },
      yaxis: {
        title: '<b>Nombre d\'utilisateurs</b>',
        // type: 'log',
      },
    };
    const trace = [trace1, trace2];
    Plotly.newPlot(div, trace, layout);
  }
  
  async function main() {
    const rows = await d3.csv('dataset/Spotify_users_data.csv');
    console.log(rows);
    const rowsFree = rows.filter((row) => (row.spotify_subscription_plan === 'Free (ad-supported)'));
    console.log(rowsFree);
    const rowsPro = rows.filter((row) => (row.spotify_subscription_plan === 'Premium (paid subscription)'));
    console.log(rowsPro);
    const div = document.getElementById('contexteEcoutePourcent');
    const compteCategorie = Array.from(new Set(unpack(rows, 'music_lis_frequency')));
    console.log(compteCategorie);
  
    const nbUtiContexte = {};
    compteCategorie.forEach((contexte) => { // parcours chaque categorie
      nbUtiContexte[contexte] = 0; // initialise le compteurs
      rows.forEach((row) => { // pour chaque ligne
        if (row.music_lis_frequency === contexte) { // si dans la ligne -> bonne catégorie
          nbUtiContexte[contexte] += 1; // on ajoute 1
        }
      });
    });
    console.log(nbUtiContexte); // on vérifie le nb d'uti dans chaque catégorie
  
    const nbUtiContexteFree = {};
    compteCategorie.forEach((contexte) => { // parcours chaque categorie
      nbUtiContexteFree[contexte] = 0; // initialise le compteurs
      rowsFree.forEach((row) => { // pour chaque ligne
        if (row.music_lis_frequency === contexte) { // si dans la ligne -> bonne catégorie
          nbUtiContexteFree[contexte] += 1; // on ajoute 1
        }
      });
    });
    console.log(nbUtiContexteFree); // on vérifie le nb d'uti dans chaque catégorie
  
    const nbUtiContextePro = {};
    compteCategorie.forEach((contexte) => { // parcours chaque categorie
      nbUtiContextePro[contexte] = 0; // initialise le compteurs
      rowsPro.forEach((row) => { // pour chaque ligne
        if (row.music_lis_frequency === contexte) { // si dans la ligne -> bonne catégorie
          nbUtiContextePro[contexte] += 1; // on ajoute 1
        }
      });
    });
    console.log(nbUtiContextePro); // on vérifie le nb d'uti dans chaque catégorie
  
    const pourcentageFree = {};
    compteCategorie.forEach((contexte) => { // parcours chaque categorie
      pourcentageFree[contexte] = 0; // initialise le compteurs
      rowsFree.forEach((row) => { // pour chaque ligne
        if (row.music_lis_frequency === contexte) { // si dans la ligne -> bonne catégorie
          pourcentageFree[contexte] = ((nbUtiContexteFree[contexte] / nbUtiContexte[contexte] * 100 )); // on ajoute 1
        }
      });
    });
    console.log(pourcentageFree); // on vérifie le nb d'uti dans chaque catégorie
  
    const pourcentagePro = {};
    compteCategorie.forEach((contexte) => { // parcours chaque categorie
      pourcentagePro[contexte] = 0; // initialise le compteurs
      rowsPro.forEach((row) => { // pour chaque ligne
        if (row.music_lis_frequency === contexte) { // si dans la ligne -> bonne catégorie
          pourcentagePro[contexte] = ((nbUtiContextePro[contexte] / nbUtiContexte[contexte] * 100 )); // on ajoute 1
        }
      });
    });
    console.log(pourcentagePro); // on vérifie le nb d'uti dans chaque catégorie
  
    drawListeningContextPourcent(div, pourcentageFree, pourcentagePro, nbUtiContexte);
    // fonction de filtre
  }
  
  main();
  
  function drawListeningContext(div, rowsFree, rowsPro) {
    // const xValue = unpack(rows, 'music_lis_frequency');
    const FlistFree = unpack(rowsFree, 'music_lis_frequency');
    const FlistPro = unpack(rowsPro, 'music_lis_frequency');
    const trace1 = {
      // x: xValue,
      x: FlistFree,
      type: 'histogram',
      hovertemplate: '%{y} users Freemium listen %{x} ',
      name: 'Contexte d\'écoute Freemium',
      marker: {
        color: 'rgb(163,165,217)',
        line: {
          color: 'rgb(141,143,188)',
          width: 1.5,
        },
      },
    };
    const trace2 = {
      // x: xValue,
      x: FlistPro,
      type: 'histogram',
      hovertemplate: '%{y} users Premium listen %{x} ',
      name: 'Contexte d\'écoute Premium',
      marker: {
        color: 'rgb(110,101,166)',
        line: {
          color: 'rgb(98,93,168)',
          width: 1.5,
        },
      },
    };
    const layout = {
      width: 800,
      height: 800,
      title: '<b>Contexte d\'écoute</b> en fonction de <b>l\'abonnement</b>',
      barmode: 'stack',
      bargap: 0.3,
      xaxis: {
        title: '<b>Contexte d\'écoute</b>',
        tickangle: -75,
        automargin: true,
        // type: 'log',
      },
      yaxis: {
        title: '<b>Nombre d\'utilisateurs</b>',
        // type: 'log',
      },
    };
    const trace = [trace1, trace2];
    Plotly.newPlot(div, trace, layout);
    console.log(FlistFree);
    console.log(FlistPro);
  }
  
  async function main2() {
    const rows = await d3.csv('dataset/Spotify_users_data.csv');
    console.log(rows);
    const rowsFree = rows.filter((row) => (row.spotify_subscription_plan === 'Free (ad-supported)'));
    console.log(rowsFree);
    const rowsPro = rows.filter((row) => (row.spotify_subscription_plan === 'Premium (paid subscription)'));
    console.log(rowsPro);
    const div = document.getElementById('contexteEcoute');
    drawListeningContext(div, rowsFree, rowsPro);
    // fonction de filtre
  }
  
  main2();
  
  const myColors = {
    'Podcast': 'rgb(163,165,217)',
    'Music': 'rgb(110,101,166)',
  };
  
  function drawFavContentFree(div, rowsFree) {
    const TypeFree = unpack(rowsFree, 'preferred_listening_content');
  
    const trace1 = {
      labels: TypeFree,
      type: 'pie',
      marker: {
        colors: TypeFree.map((value) => myColors[value]),
      },
    };
    const layout = {
      width: 800,
      height: 500,
      title: 'Type d\'écoute des <b>utilisateurs Freemium</b>',
    };
    Plotly.newPlot(div, [trace1], layout);
  }
  
  function drawFavContentPro(div, rowsPro) {
    const TypePro = unpack(rowsPro, 'preferred_listening_content');
    const trace2 = {
      labels: TypePro,
      type: 'pie',
      marker: {
        colors: TypePro.map((value) => myColors[value]),
      },
    };
    const layout = {
      width: 800,
      height: 500,
      title: 'Type d\'écoute des <b>utilisateurs Premium</b>',
    };
    Plotly.newPlot(div, [trace2], layout);
  }
  async function main3() {
    const rows = await d3.csv('dataset/Spotify_users_data.csv');
    console.log(rows);
    const rowsFree = rows.filter((row) => (row.spotify_subscription_plan === 'Free (ad-supported)'));
    const rowsPro = rows.filter((row) => (row.spotify_subscription_plan === 'Premium (paid subscription)'));
    console.log(rowsFree);
    const div = document.getElementById('typeEcouteFree');
    const div2 = document.getElementById('typeEcoutePro');
    drawFavContentFree(div, rowsFree);
    drawFavContentPro(div2, rowsPro);
    // fonction de filtre
  }
  
  main3();



    //////////////////////////////////////NAYIM ////////////////////////////////////


    async function main() {
        const rows = await d3.csv("../dataset/spotify_users/Spotify_users_data.csv");
       console.log(rows);
    
       const rowsFree = rows.filter((row)=> {return row.spotify_subscription_plan === 'Free (ad-supported)'});
       console.log(rowsFree);
       const rowsPremium = rows.filter((row)=> {return row.spotify_subscription_plan === 'Premium (paid subscription)'});
       console.log(rowsPremium);
       const nayimPlot = document.getElementById('Nayimplot');
       const nayimPlot2 = document.getElementById('Nayimplot2');
       const nayimPlot3 = document.getElementById('Nayimplot3');
       const nayimPlot6 = document.getElementById('Nayimplot4');
    
       Nayimdraw(nayimPlot, rowsFree);
       Nayimdraw2(nayimPlot2, rowsPremium);
       Nayimdraw3(nayimPlot3, rowsPremium);
       Nayimdraw6(nayimPlot6, rowsFree);
       
    };
    
    
    
    
    function unpack(rows, key) { //on veut extraire les key du tableau rows
        return (rows.map ((row) => { //map fais parcourir le tableau
     //console.log(rows);
            return row[key]; //ca stock  les resulats dans un tableau
        }))
    };
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////GRAPHIQUE YES OR NO GRATUIT TO PREMIUM ////////////////////////////////////////////////
    
    function Nayimdraw (div, rowsFree)
     {
    const volonter = unpack (rowsFree, 'premium_sub_willingness');
    const trace = {
        x : volonter,
        type : 'histogram',
    
    };
    
    const layout = {
        width: 800,
        height: 500, 
        title: 'Pars d/utilisateur gratuit ayant envie ou non de prendre le premium',
        bargap: 0.2, 
    }
    Plotly.newPlot(div, [trace], layout);
     }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////GRAPHIQUE YES OR NO PREMIUM TO RENOUVELER EN PREMIUM ////////////////////////////////////////////////
    
    
     function Nayimdraw2 (div, rowsPremium)
     {
    const volonter2 = unpack (rowsPremium, 'premium_sub_willingness');
    // const customColors1 = {
    //        'Yes': 'hsl(34,0%,66%)',
    //        'No': 'hsl(45,54%,78%)',
    //     }
    const trace2 = {
        x : volonter2,
        type : 'histogram',
       
    };
    
    const layout = {
        width: 800,
        height: 500, 
        title: 'Pars d/utilisateur Premium ayant envie de renouveler ou non ',
        bargap: 0.2, 
    }
    Plotly.newPlot(div, [trace2], layout);
     }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////GRAPHIQUE AGE PREMIUM ////////////////////////////////////////////////
    
    function Nayimdraw3 (div, rowsPremium) {
        const Age = unpack(rowsPremium, 'Age');
        const trace3 = {
            labels : Age,
            type : 'pie',
            hovertemplate: '%{label} d utilisateurs'
        };
    
        const layout3 = {
            width: 800,
            height: 500,
            title: 'Pars d utilisateur ayant un abonnememnt premium selon l age',
            
        }
        Plotly.newPlot(div, [trace3], layout3);
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////GRAPHIQUE AGE GRATUIT ////////////////////////////////////////////////
    
    function Nayimdraw6(div, rowsFree) {
        const Age2 = unpack(rowsFree, 'Age');
        const trace4 = {
            labels : Age2,
            type : 'pie',
            hovertemplate: '%{label} d utilisateurs'
        };
    
        const layout4 = {
            width: 800,
            height: 500,
            title: 'Pars d utilisateur ayant un abonnement gratuit selon l age',
        }
        Plotly.newPlot(div, [trace4], layout4);
    }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
        //////////////////////////////////////FIN NAYIM ////////////////////////////////////
    
    main();
    
    
    
    